Source: didjvu
Maintainer: Aleš Kapica <ales@kapica.cz>
Section: graphics
Priority: optional
Build-Depends:
 dh-python,
 djvulibre-bin,
 python3-gamera (>= 4.1~),
 gir1.2-gexiv2-0.10 (>= 0.10.3),
 python3-py3exiv2,
 python3-gi,
 python3-nose,
 python3-pil
Standards-Version: 4.4.1
Homepage: http://jwilk.net/software/didjvu
Vcs-Git: https://salsa.debian.org/want/didjvu.git
Vcs-Browser: https://salsa.debian.org/want/didjvu

Package: python3-didjvu
Architecture: all
Depends:
 djvulibre-bin,
 minidjvu,
 python (>= 3.6) | python3-argcomplete,
 python3-gamera (>= 4.1~),
 python3-pil,
 ${misc:Depends},
 ${python:Depends}
Suggests:
 gir1.2-gexiv2-0.10 (>= 0.10.3), python3-gi
Description: DjVu encoder with foreground/background separation
 The DjVu graphics format is very effective because it uses
 multiple layers which are differently compressed. For the
 derivation of the bitonal foreground layer ("mask") of
 (scanned document) images ("segmentation") didjvu uses the
 Gamera framework.
 .
 In the same process didjvu encodes the foreground mask together
 with the background counterpart into DjVu. That's also possible
 for a stack of images which can be processes into a bundled DjVu
 container very easily. The results are suitable for getting high-
 quality OCR data even from problematic scans from old documents.
 Furthermore, didjvu is capable of processing bitonal segmentation on
 colour scans.
